# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=py3-xmlschema
pkgver=3.4.1
pkgrel=0
pkgdesc="XML schema validator and conversion library"
url="https://github.com/sissaschool/xmlschema"
arch="noarch"
license="MIT"
depends="py3-elementpath"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-jinja2"
options="net" # tests access xml.xsd from w3.org
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/sissaschool/xmlschema/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/xmlschema-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
6f4eba3db2f8213db7f04df8223850168f90497e2daa36443d1236baadf5bc4cc24e1b1cc6c557019f6200e5fe436edf60dac2c3794b9dfddda6d76f18df2bc6  py3-xmlschema-3.4.1.tar.gz
"
