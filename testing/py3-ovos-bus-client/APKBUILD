# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-ovos-bus-client
pkgver=0.0.10
pkgrel=0
pkgdesc="A simple interface for the mycroft messagebus and can be used to connect to ovos"
url="https://github.com/OpenVoiceOS/ovos-bus-client"
arch="noarch"
license="Apache-2.0"
depends="
	py3-orjson
	py3-ovos-config
	py3-ovos-utils
	py3-pyee
	py3-websocket-client
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/OpenVoiceOS/ovos-bus-client/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/ovos-bus-client-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# Disabled tests have broken imports
	.testenv/bin/python3 -m pytest  -k 'not test_class_patching and not test_inheritance and not test_create_client' \
		--ignore test/unittests/test_compat.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
3c551897da0726fddfaf9fb6a0275f7db7afd435dfb3bcc6128ddf7b9abd11d9caf8cac48e681116bdcf0b122d2b578b436a5ce720492bce1f475bf7386564d5  py3-ovos-bus-client-0.0.10.tar.gz
"
